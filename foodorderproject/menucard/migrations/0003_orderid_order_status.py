# Generated by Django 4.1.4 on 2022-12-13 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menucard', '0002_orderid_variable'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderid',
            name='order_status',
            field=models.CharField(choices=[('Progressing', 'Progressing'), ('Prepred', 'Prepared'), ('Completed', 'Completed')], default='Progressing', max_length=100),
        ),
    ]
